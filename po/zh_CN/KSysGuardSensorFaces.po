msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-13 00:48+0000\n"
"PO-Revision-Date: 2023-01-12 10:31\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/libksysguard/KSysGuardSensorFaces.pot\n"
"X-Crowdin-File-ID: 12260\n"

#: ConfigAppearance.qml:107
#, kde-format
msgid "Presets:"
msgstr "预设："

#: ConfigAppearance.qml:111
#, kde-format
msgid "Load Preset..."
msgstr "加载预设..."

#: ConfigAppearance.qml:116
#, kde-format
msgid "Get new presets..."
msgstr "获取新预设..."

#: ConfigAppearance.qml:128
#, kde-format
msgid "Save Settings As Preset"
msgstr "保存设置为预设"

#: ConfigAppearance.qml:138
#, kde-format
msgid "Title:"
msgstr "标题："

#: ConfigAppearance.qml:144
#, kde-format
msgid "Show Title"
msgstr "显示标题"

#: ConfigAppearance.qml:149 facepackages/facegrid/contents/ui/Config.qml:54
#, kde-format
msgid "Display Style:"
msgstr "显示样式："

#: ConfigAppearance.qml:169
#, kde-format
msgid "Get New Display Styles..."
msgstr "获取新显示样式..."

#: ConfigAppearance.qml:179
#, kde-format
msgid "Minimum Time Between Updates:"
msgstr "更新最小间隔："

#: ConfigAppearance.qml:188
#, kde-format
msgid "No Limit"
msgstr "无限制"

#. i18ndp doesn't handle floats :(
#: ConfigAppearance.qml:192
#, kde-format
msgid "1 second"
msgstr "1 秒"

#: ConfigAppearance.qml:194
#, kde-format
msgid "%1 seconds"
msgstr "%1 秒"

#: ConfigSensors.qml:131
#, kde-format
msgid "Total Sensor"
msgid_plural "Total Sensors"
msgstr[0] "全部传感器"

#: ConfigSensors.qml:150
#, kde-format
msgid "Sensors"
msgstr "传感器"

#: ConfigSensors.qml:176
#, kde-format
msgid "Text-Only Sensors"
msgstr "仅文本传感器"

#: facepackages/barchart/contents/ui/Config.qml:35
#: facepackages/linechart/contents/ui/Config.qml:47
#: facepackages/piechart/contents/ui/Config.qml:34
#, kde-format
msgid "Show Sensors Legend"
msgstr "显示传感器图例"

#: facepackages/barchart/contents/ui/Config.qml:39
#, kde-format
msgid "Stacked Bars"
msgstr "堆叠柱状图"

#: facepackages/barchart/contents/ui/Config.qml:43
#: facepackages/linechart/contents/ui/Config.qml:59
#, kde-format
msgid "Show Grid Lines"
msgstr "显示网格线"

#: facepackages/barchart/contents/ui/Config.qml:47
#: facepackages/linechart/contents/ui/Config.qml:63
#, kde-format
msgid "Show Y Axis Labels"
msgstr "显示 Y 轴标签"

#: facepackages/barchart/contents/ui/Config.qml:51
#: facepackages/piechart/contents/ui/Config.qml:69
#, kde-format
msgid "Automatic Data Range"
msgstr "自动数据范围"

#: facepackages/barchart/contents/ui/Config.qml:55
#: facepackages/piechart/contents/ui/Config.qml:73
#, kde-format
msgid "From:"
msgstr "来源："

#: facepackages/barchart/contents/ui/Config.qml:62
#: facepackages/piechart/contents/ui/Config.qml:80
#, kde-format
msgid "To:"
msgstr "目标："

#: facepackages/colorgrid/contents/ui/Config.qml:25
#, kde-format
msgid "Use sensor color:"
msgstr "使用传感器颜色："

#: facepackages/colorgrid/contents/ui/Config.qml:30
#: facepackages/facegrid/contents/ui/Config.qml:35
#, kde-format
msgid "Number of Columns:"
msgstr "列数："

#: facepackages/colorgrid/contents/ui/Config.qml:37
#: facepackages/facegrid/contents/ui/Config.qml:42
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "自动"

#: facepackages/linechart/contents/ui/Config.qml:42
#, kde-format
msgid "Appearance"
msgstr "外观"

#: facepackages/linechart/contents/ui/Config.qml:51
#, kde-format
msgid "Stacked Charts"
msgstr "堆叠柱状图"

#: facepackages/linechart/contents/ui/Config.qml:55
#, kde-format
msgid "Smooth Lines"
msgstr "平滑线条"

#: facepackages/linechart/contents/ui/Config.qml:67
#, kde-format
msgid "Fill Opacity:"
msgstr "填充不透明度："

#: facepackages/linechart/contents/ui/Config.qml:73
#, kde-format
msgid "Data Ranges"
msgstr "数据范围"

#: facepackages/linechart/contents/ui/Config.qml:78
#, kde-format
msgid "Automatic Y Data Range"
msgstr "自动 Y 轴数据范围"

#: facepackages/linechart/contents/ui/Config.qml:82
#, kde-format
msgid "From (Y):"
msgstr "来源 (Y)："

#: facepackages/linechart/contents/ui/Config.qml:89
#, kde-format
msgid "To (Y):"
msgstr "目标 (Y)："

#: facepackages/linechart/contents/ui/Config.qml:99
#, kde-format
msgid "Amount of History to Keep:"
msgstr "要保留的历史记录数量："

#: facepackages/linechart/contents/ui/Config.qml:102
#, kde-format
msgctxt "%1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 秒"

#: facepackages/piechart/contents/ui/Config.qml:38
#, kde-format
msgid "Start from Angle:"
msgstr "起始角度："

#: facepackages/piechart/contents/ui/Config.qml:43
#, kde-format
msgctxt "angle degrees"
msgid "%1°"
msgstr "%1°"

#: facepackages/piechart/contents/ui/Config.qml:46
#: facepackages/piechart/contents/ui/Config.qml:59
#, kde-format
msgctxt "angle degrees"
msgid "°"
msgstr "°"

#: facepackages/piechart/contents/ui/Config.qml:51
#, kde-format
msgid "Total Pie Angle:"
msgstr "饼状图总角度："

#: facepackages/piechart/contents/ui/Config.qml:56
#, kde-format
msgctxt "angle"
msgid "%1°"
msgstr "%1°"

#: facepackages/piechart/contents/ui/Config.qml:64
#, kde-format
msgid "Rounded Lines"
msgstr "圆滑线条"

#: facepackages/textonly/contents/ui/Config.qml:24
#, kde-format
msgid "Group sensors based on the value of the total sensors."
msgstr "传感器分组，基于所有传感器的数值"

#: import/Choices.qml:53
#, kde-format
msgctxt "@label"
msgid "Click to select a sensor…"
msgstr "点击选择传感器…"

#: import/Choices.qml:368
#, kde-format
msgid "Search..."
msgstr "搜索..."

#: import/Choices.qml:381
#, kde-format
msgctxt "@action:button"
msgid "Back"
msgstr "返回"

#: packagestructure/sensorfacepackage.cpp:25
#, kde-format
msgid "User Interface"
msgstr "用户界面"

#: packagestructure/sensorfacepackage.cpp:29
#, kde-format
msgid ""
"The compact representation of the sensors plasmoid when collapsed, for "
"instance in a panel."
msgstr "传感器 Plasma 小程序在面板折叠等情况下的紧凑式显示效果。"

#: packagestructure/sensorfacepackage.cpp:34
#, kde-format
msgid "The representation of the plasmoid when it's fully expanded."
msgstr "传感器 Plasma 小程序在完全展开时的显示效果。"

#: packagestructure/sensorfacepackage.cpp:37
#, kde-format
msgid "The optional configuration page for this face."
msgstr "此面板的可选配置页面。"

#: packagestructure/sensorfacepackage.cpp:39
#, kde-format
msgid "Configuration support"
msgstr "配置支持"

#: packagestructure/sensorfacepackage.cpp:40
#, kde-format
msgid "KConfigXT xml file for face-specific configuration options."
msgstr "KConfigXT XML 文件，用于面板的配置选项。"

#: packagestructure/sensorfacepackage.cpp:44
#, kde-format
msgid "The configuration file that describes face properties and capabilities."
msgstr "描述面板属性和功能的配置文件。"

#: SensorFaceController.cpp:434
#, kde-format
msgid "System Monitor Sensor"
msgstr "系统监视传感器"
