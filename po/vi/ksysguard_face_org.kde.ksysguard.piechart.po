# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-09 00:44+0000\n"
"PO-Revision-Date: 2022-10-31 16:41+0100\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.08.1\n"

#: contents/ui/Config.qml:34
#, kde-format
msgid "Show Sensors Legend"
msgstr "Hiện chú thích cảm biến"

#: contents/ui/Config.qml:38
#, kde-format
msgid "Start from Angle:"
msgstr "Bắt đầu từ góc:"

#: contents/ui/Config.qml:43
#, kde-format
msgctxt "angle degrees"
msgid "%1°"
msgstr "%1°"

#: contents/ui/Config.qml:46 contents/ui/Config.qml:59
#, kde-format
msgctxt "angle degrees"
msgid "°"
msgstr "°"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Total Pie Angle:"
msgstr "Tổng góc bánh:"

#: contents/ui/Config.qml:56
#, kde-format
msgctxt "angle"
msgid "%1°"
msgstr "%1°"

#: contents/ui/Config.qml:64
#, kde-format
msgid "Rounded Lines"
msgstr "Các đường bo tròn"

#: contents/ui/Config.qml:69
#, kde-format
msgid "Automatic Data Range"
msgstr "Khoảng dữ liệu tự động"

#: contents/ui/Config.qml:73
#, kde-format
msgid "From:"
msgstr "Từ:"

#: contents/ui/Config.qml:80
#, kde-format
msgid "To:"
msgstr "Đến:"
